console.log("Hello World!");


let student = [];

function addStudents(add) {	
	let upperCased = add.charAt(0).toUpperCase() + add.slice(1);
	let added = student.push(upperCased);
	console.log(`${upperCased} has been added to enrolled list.`);
}
function countStudents() {
	let count = student.length;
	console.log(`There are currently ${count} students enrolled in the system.`);
}
function printStudents() {
	let sorted = student.sort();
	console.log(`Here are the enrollees in the system:`);
	let printSorted = sorted.forEach(name => console.log(name.charAt(0).toUpperCase() + name.slice(1)));
}
function findStudents(studentName) {
	let filtered = student.filter(
		function(name) {
			return name.charAt(0).toLowerCase().indexOf(studentName.toLowerCase()) !== -1;
		}
	);
	if(filtered.length == 1 && filtered != studentName) {
		console.log(`${filtered} is an enrollee.`)
	} else if(filtered.length > 1) {
		console.log(`${filtered} are enrollees.`);
	} else {
		console.log(`${studentName} not found in the system.`)
	}
}
